﻿using System;

namespace ShiftArrayElements
{
    public static class RecursiveEnumShifter
    {
        public static int[] Shift(int[] source, Direction[] directions)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (directions is null)
            {
                throw new ArgumentNullException(nameof(directions));
            }

            if (CheckDirections(directions) == -1)
            {
                throw new InvalidOperationException(nameof(directions));
            }

            if (source.Length == 0)
            {
                return source;
            }

            if (directions.Length == 0)
            {
                return source;
            }

            return Shift(ShiftOneStep(source, directions[0]), directions[1..]);

            int[] ShiftOneStep(int[] source, Direction direction)
            {
                if (source is null)
                {
                    throw new ArgumentNullException(nameof(source));
                }

                if (direction == Direction.Left)
                {
                    int firstElement = source[0];
                    Array.Copy(source, 1, source, 0, source.Length - 1);
                    source[^1] = firstElement;
                    return source;
                }

                int lastElement = source[^1];
                Array.Copy(source, 0, source, 1, source.Length - 1);
                source[0] = lastElement;
                return source;
            }

            int CheckDirections(Direction[] directions)
            {
                if (directions is null)
                {
                    throw new ArgumentNullException(nameof(directions));
                }

                if (directions.Length == 0)
                {
                    return 0;
                }

                if (directions[0] == Direction.Left || directions[0] == Direction.Right)
                {
                    return CheckDirections(directions[1..]);
                }

                return -1;
            }
        }
    }
}
