﻿using System;

namespace ShiftArrayElements
{
    public static class RecursiveShifter
    {
        public static int[] Shift(int[] source, int[] iterations)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (iterations is null)
            {
                throw new ArgumentNullException(nameof(iterations));
            }

            int leftCount = FindLeftStep(iterations);
            int rightCount = FindRightStep(iterations);

            if (leftCount == rightCount)
            {
                return source;
            }

            if (leftCount > rightCount)
            {
                return ShiftAllSteps(source, leftCount - rightCount, Direction.Left);
            }

            return ShiftAllSteps(source, rightCount - leftCount, Direction.Right);

            int FindLeftStep(int[] iteration)
            {
                if (iteration is null)
                {
                    throw new ArgumentNullException(nameof(iteration));
                }

                if (iteration.Length == 0)
                {
                    return 0;
                }

                if (iteration.Length == 1)
                {
                    return iteration[0];
                }

                return FindLeftStep(iteration[2..]) + iteration[0];
            }

            int FindRightStep(int[] iteration)
            {
                if (iteration is null)
                {
                    throw new ArgumentNullException(nameof(iteration));
                }

                if (iteration.Length == 0 || iteration.Length == 1)
                {
                    return 0;
                }

                iteration = iteration[1..];

                return FindRightStep(iteration[1..]) + iteration[0];
            }

            int[] ShiftOneStep(int[] source, int index, Direction direction)
            {
                if (source is null)
                {
                    throw new ArgumentNullException(nameof(source));
                }

                if (index == source.Length - 1)
                {
                    return source;
                }

                if (direction == Direction.Left)
                {
                    int tmpLeft = source[index];
                    source[index] = source[index + 1];
                    source[index + 1] = tmpLeft;
                    return ShiftOneStep(source, index + 1, direction);
                }

                int tmpRight = source[^(index + 1)];
                source[^(index + 1)] = source[^(index + 2)];
                source[^(index + 2)] = tmpRight;
                return ShiftOneStep(source, index + 1, direction);
            }

            int[] ShiftAllSteps(int[] source, int count, Direction direction)
            {
                if (source is null)
                {
                    throw new ArgumentNullException(nameof(source));
                }

                if (count == 0)
                {
                    return source;
                }

                if (direction == Direction.Left)
                {
                    source = ShiftOneStep(source, 0, Direction.Left);
                    return ShiftAllSteps(source, count - 1, direction);
                }

                source = ShiftOneStep(source, 0, Direction.Right);
                return ShiftAllSteps(source, count - 1, direction);
            }
        }
    }
}
